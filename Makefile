install:
	python setup.py install

clean:
	rm -f MANIFEST .coverage
	rm -rf .tox brevduva.egg-info
	rm -f **/*.pyc
	rm -rf **/{build,__pycache__,}
