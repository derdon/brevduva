Brevduva
========

About
-----

brevduva is a graphical email client that can be completely used by your
keyboard. It aims to be minimalistic, easy to use and do only one thing: email.
No calendar, no to do list, no address book. If you want a do-everything-app,
I have to disappoint you: brevduva is the wrong kind of software for you.

Usage
-----

n
    compose a **n**\ew mail

a
    **a**\nswer to the currently selected mail

f
    **f**\etch new mails from the currently selected account

F
    **f**\etch new mails from all accounts

r
    **r**\eply to the selected mail(s)

R
    **R**\eply to all mails in the currently selected folder(s)

q
    **q**\uit brevduva

\+
    add a new mail account

\-
    remove the selected account(s), selected folder(s), or selected
    mail(s), respectively

s
    change **s**\ettings

/
    search all mails

\*
    mark or unmark the selected mail(s)

TAB
    cycle through the three views: accounts / folders, mail info, and mail
    content

Install
-------

Before you can install brevduva, you need to install PyGTK3. Instructions
for that can be found at
http://python-gtk-3-tutorial.readthedocs.org/en/latest/install.html.

After that, brevduva can either be installed via pip::

    pip install brevduva

Or you use the following command from the project directory of brevduva::

    make install

Both commands usually require root previliges.
That's it! Now you can call brevduva from the command line to launch it
(menu entries are not supported yet, sorry).

Running the tests
-----------------

To be able to run the tests, you need to have installed brevduva first.
You also need the latest tox version. And by that, I mean really latest
(newer than version 1.4.2, so currently you'd have to clone the repo from
https://bitbucket.org/hpk42/tox).
After that, you can run the tests by calling ``tox`` from the
command line.
