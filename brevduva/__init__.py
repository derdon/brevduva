from os import getenv
from os.path import expanduser, join as join_path


CONFIG_DIR = getenv(
    'BREVDUVACONFIG', expanduser(join_path('~', '.config', 'brevduva')))

ACCOUNTSFILE = join_path(CONFIG_DIR, "accounts.ini")
