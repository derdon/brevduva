from __future__ import unicode_literals

from collections import namedtuple
from ConfigParser import RawConfigParser
from imaplib import IMAP4_PORT, IMAP4_SSL_PORT

Account = namedtuple(
    "Account", ["imapserver", "port", "name", "autologin", "ssl"])


class AccountNotFoundError(Exception):
    pass


class AccountAlreadyExistsError(Exception):
    pass


class Accounts(dict):
    def __repr__(self):
        return "<{0}({1})>".format(
            self.__class__.__name__, dict.__repr__(self))

    def add(self, accountname, account, overwrite=False):
        if not overwrite:
            if accountname in self:
                raise AccountAlreadyExistsError()
        self[accountname] = account

    def remove(self, accountname):
        try:
            return self.pop(accountname)
        except KeyError:
            raise AccountNotFoundError()

    def edit(self, accountname, new_account):
        if accountname not in self:
            raise AccountNotFoundError()
        self[accountname] = new_account


class ConfigBasedAccountManager(object):
    defaults = {"autologin": "no", "ssl": "yes"}
    config = RawConfigParser(defaults)

    def __init__(self, fp):
        self.fp = fp
        self.accounts = Accounts()

    def save_changes(self):
        self.config.write(self.fp)

    def close_accounts_file(self):
        self.fp.close()

    def read_config(self):
        """parse the configuration file. This method must be called before the
        other methods can be used.

        """
        config = self.config
        config.readfp(self.fp)
        temp_accounts = []
        for accountname in config.sections():
            name = config.get(accountname, "name")
            imapserver = config.get(accountname, "imapserver")
            autologin = config.getboolean(accountname, "autologin")
            use_ssl = config.getboolean(accountname, "ssl")
            if config.has_option(accountname, "port"):
                port = config.getint(accountname, "port")
            else:
                port = IMAP4_SSL_PORT if use_ssl else IMAP4_PORT
            account = Account(imapserver, port, name, autologin, use_ssl)
            temp_accounts.append((accountname, account))
        self.accounts.update(temp_accounts)

    def add(self, accountname, account, overwrite=False):
        self.config.add_section(accountname)
        for option in "name", "imapserver", "autologin", "ssl":
            # FIXME: getattr may raise AttributeError!
            value = getattr(account, option)
            # convert True to yes and False to no
            if value in (True, False):
                value = 'yes' if value else 'no'
            self.config.set(accountname, option, str(value))
        port = account.port
        if not port:
            port = IMAP4_SSL_PORT if account.ssl else IMAP4_PORT
        self.config.set(accountname, "port", str(port))
        self.accounts.add(accountname, account, overwrite)
