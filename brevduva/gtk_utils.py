from gi.repository import Gtk


class Box(Gtk.Box):
    def __init__(self, orientation):
        super(Box, self).__init__(orientation=orientation)
        self.set_homogeneous(False)
        self.set_spacing(0)
        self.orientation = orientation

    def pack(self, child):
        # child, expand, fill, padding
        self.pack_start(child, True, True, 0)


class VBox(Box):
    def __init__(self):
        super(VBox, self).__init__(Gtk.Orientation.VERTICAL)


class HBox(Box):
    def __init__(self):
        super(HBox, self).__init__(Gtk.Orientation.HORIZONTAL)


def toggle_treeview_path(treeview, treepath, recursive=False):
    if treeview.row_expanded(treepath):
        treeview.collapse_row(treepath)
    else:
        treeview.expand_row(treepath, open_all=recursive)


class ErrorMessageDialog(Gtk.MessageDialog):
    def __init__(self, errmsg):
        Gtk.MessageDialog.__init__(
            self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, errmsg)
