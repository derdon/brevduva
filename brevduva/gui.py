from __future__ import unicode_literals

import email
import imaplib  # only required for exception handling
from imaplib import IMAP4_PORT, IMAP4_SSL_PORT

import imapclient
from gi.repository import Gtk

from brevduva import ACCOUNTSFILE
from brevduva.accounts import Account, ConfigBasedAccountManager,\
    AccountAlreadyExistsError
from brevduva.gtk_utils import VBox, HBox, ErrorMessageDialog, toggle_treeview_path


class MainWindow(Gtk.Window):
    def __init__(self, accountsfile):
        Gtk.Window.__init__(self, title="Brevduva")
        # NOTE: if there are multiple sections with the same name, it is not
        # defined which ones are ignored and which ones are interpreted!
        self.account_manager = ConfigBasedAccountManager(accountsfile)
        self.account_manager.read_config()

        # keys are acount names, values are imapclient.IMAPClient instances
        self.active_accounts = {}

        # imapclient.IMAPClient instance, None if the currently active account
        # is logged out
        self.cur_account = None

        # stores will be set by `init_folders_tree` and `init_mail_list`,
        # respectively
        self.folders = Gtk.TreeView()
        self.mails = Gtk.TreeView()

        # TODO: TextView
        mail_content = Gtk.Label("MAIL CONTENT!")

        mail_box = VBox()
        mail_box.pack(self.mails)
        mail_box.pack(mail_content)

        main_box = HBox()
        main_box.pack(self.folders)
        main_box.pack(mail_box)

        self.add(main_box)

        self.connect("key-press-event", self.on_key_press)
        self.connect("delete-event", self.shutdown)

    def init_folders_tree(self):
        """"""
        store = Gtk.TreeStore(str)
        self.folders.set_model(store)
        self.folders.set_headers_visible(False)
        self.folders.append_column(
            Gtk.TreeViewColumn("Folders", Gtk.CellRendererText(), text=0))
        # populate the tree store
        for name, account in self.account_manager:
            parent = store.append(None, [name])
        # call self.on_folder_tree_changed if an entry is clicked or selected
        # via one of the arrow keys
        select = self.folders.get_selection()
        select.connect("changed", self.on_folder_tree_changed)
        # call self.on_account_enter if an entry is double clicked or if the
        # enter key is pressed after an entry was selected
        self.folders.connect("row-activated", self.on_account_enter)

    def init_mail_list(self):
        columns = [
            Gtk.TreeViewColumn("Subject", Gtk.CellRendererText(), text=0),
            Gtk.TreeViewColumn("From", Gtk.CellRendererText(), text=1),
            Gtk.TreeViewColumn("Date", Gtk.CellRendererText(), text=2)]
        self.mails.set_store(Gtk.TreeStore(str, str, str))
        self.mails.set_headers_visible(False)
        for column in columns:
            self.mails.append_column(column)
        #select = self.mails.get_selection()
        #select.connect("changed", self.on_mail_tree_changed)

    def perform_autologin(self):
        # TODO: iterate over the tree store of the folders
        for name, account, parent in to_be_logged_in:
            self.login(name, account, parent)

    def login(self, name, account, parent):
        self.cur_account = imapclient.IMAPClient(
            account.imapserver, ssl=account.ssl)
        login_dialog = LoginDialog(self, account.name)
        response = login_dialog.run()
        if response == Gtk.ResponseType.OK:
            password = login_dialog.get_password()
        else:
            password = None
        login_dialog.destroy()
        if password is None:
            return
        try:
            self.cur_account.login(account.name, password)
        except imaplib.IMAP4.error:
            errmsg = "Could not login. Check your username and password."
            dialog = ErrorMessageDialog(errmsg)
            dialog.run()
            dialog.destroy()
            return
        self.active_accounts[name] = self.cur_account
        store = self.folders.get_model()
        for flags, _, folder in self.cur_account.list_folders():
            supports_child_mailboxes = 'CHILDREN' in\
                self.cur_account.capabilities()
            if supports_child_mailboxes and u"\\HasChildren" in flags:
                parent = store.append(parent, [folder])
            else:
                store.append(parent, [folder])

    def logout(self, name):
        del self.active_accounts[name]
        self.cur_account.logout()
        self.cur_account = None

    def shutdown(self, _, event):
        for name in self.active_accounts.copy():
            self.logout(name)
        Gtk.main_quit()
        self.account_manager.close_accounts_file()
        return True

    def on_key_press(self, _, event):
        _, keyval = event.get_keyval()
        key = unichr(keyval)
        if key == u"q":
            self.shutdown(_, event)
            return True
        elif key == u"+":
            self.add_account()
        return False

    def on_folder_tree_changed(self, selection):
        store, treeiter = selection.get_selected()
        if treeiter is None:
            return
        item = store[treeiter][0]
        # filter out the username names from the treeview to get the folders
        # only
        # FIXME: causes a bug for folders with the same name as the username!!!
        if not self.account_manager.account_exists(item):
            model = self.mails.get_model()
            self.cur_account.select_folder(item.decode("utf_8"))
            data = 'BODY.PEEK[HEADER.FIELDS (SUBJECT FROM DATE)]'
            search_response = self.cur_account.search()
            fetch_response = self.cur_account.fetch(search_response, [data])
            for mail in fetch_response.itervalues():
                s = mail['BODY[HEADER.FIELDS (SUBJECT FROM DATE)]']
                msg = email.message_from_string(s)
                model.append(None, [msg['Subject'], msg['From'], msg['Date']])

    def on_account_enter(self, treeview, treepath, _):
        selection = treeview.get_selection()
        store, treeiter = selection.get_selected()
        cur_accountname = store[treeiter][0]
        # remove all folders of the currently selected account if it is already
        # logged in and log it out
        if cur_accountname in self.active_accounts:
            self.logout(cur_accountname)
            while store.iter_children(treeiter) is not None:
                store.remove(store.iter_children(treeiter))
        else:
            for name, account in self.account_manager:
                if name == cur_accountname:
                    self.login(name, account, treeiter)
                    treeview.expand_all()
                    break
            else:
                # toggle all subitems of the currently selected folder
                toggle_treeview_path(treeview, treepath, False)

    def on_mail_tree_changed(self, selection):
        # TODO: display mail content in self.mail_content
        print selection

    def add_account(self):
        dialog = AddAccountDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            accountname, account = dialog.get_account()
            try:
                self.account_manager.add_account(accountname, account)
            except AccountAlreadyExistsError:
                # TODO: nice error message dialog
                print 'dam, that account already exists!'
            self.account_manager.save_changes()
        dialog.destroy()
        # TODO: update the account list!


# TODO: add a "reset to defaults" button
class AddAccountDialog(Gtk.Dialog):
    def __init__(self, parent, **default_values):
        Gtk.Dialog.__init__(
            self, "Brevduva - Add a new account", parent, 0, (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_ADD, Gtk.ResponseType.OK))
        self.accountname_entry = Gtk.Entry()
        self.username_entry = Gtk.Entry()
        self.imap_entry = Gtk.Entry()
        self.port_entry = Gtk.Entry()
        self.autologin_checkbox = Gtk.CheckButton("Autologin")
        self.ssl_checkbox = Gtk.CheckButton("Use SSL")

        # TODO: set automatically to IMAP4_PORT if self.ssl_checkbox is
        # disabled and to IMAP4_SSL_PORT if self.ssl_checkbox is enabled
        self.port_entry.set_text(str(IMAP4_SSL_PORT))
        self.ssl_checkbox.set_active(True)

        grid = Gtk.Grid()
        grid.attach(Gtk.Label("Account name"), 0, 0, 1, 1)
        grid.attach(self.accountname_entry, 1, 0, 1, 1)
        grid.attach(Gtk.Label("Username"), 0, 1, 1, 1)
        grid.attach(self.username_entry, 1, 1, 1, 1)
        grid.attach(Gtk.Label("IMAP Server"), 0, 2, 1, 1)
        grid.attach(self.imap_entry, 1, 2, 1, 1)
        grid.attach(Gtk.Label("Port (optional)"), 0, 3, 1, 1)
        grid.attach(self.port_entry, 1, 3, 1, 1)
        grid.attach(self.autologin_checkbox, 0, 4, 2, 1)
        grid.attach(self.ssl_checkbox, 0, 5, 2, 1)
        box = self.get_content_area()
        box.add(grid)
        self.show_all()

    def get_account(self):
        accountname = self.accountname_entry.get_text()
        imap_server = self.imap_entry.get_text()
        port = self.port_entry.get_text()
        name = self.username_entry.get_text()
        autologin = bool(self.autologin_checkbox.get_state())
        ssl = bool(self.ssl_checkbox.get_state())
        return accountname, Account(imap_server, port, name, autologin, ssl)


class LoginDialog(Gtk.Dialog):
    # TODO: enable OK button only when the self._password_field is not empty!
    def __init__(self, parent, username):
        title = "Brevduva - Login: Enter password for {0}".format(username)
        Gtk.Dialog.__init__(
            self, title, parent, 0, (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_ADD, Gtk.ResponseType.OK))
        self._password_field = Gtk.Entry()
        self._password_field.set_visibility(False)
        box = self.get_content_area()
        box.add(Gtk.Label("Password: "))
        box.add(self._password_field)
        self.show_all()

    def get_password(self):
        return self._password_field.get_text()


def main():
    window = MainWindow(open(ACCOUNTSFILE))
    window.init_folders_tree()
    window.init_mail_list()
    window.perform_autologin()
    window.folders.expand_all()
    window.show_all()
    Gtk.main()
