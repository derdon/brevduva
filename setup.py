#!/usr/bin/env python
import os.path
from os import makedirs

from setuptools import setup

with open('README.rst') as fp:
    long_descr = fp.read()

# TODO: do the following only if the application is being installed
# create the dir ~/.config/brevduva if it doesn't exist yet
config_dir = os.path.expanduser(os.path.join('~', '.config', 'brevduva'))
if not os.path.exists(config_dir):
    makedirs(config_dir)

setup(
    name='brevduva',
    version='0.1dev',
    description='graphical email client',
    long_description=long_descr,
    author='Simon Liedtke',
    author_email='liedtke.simon@googlemail.com',
    packages=['brevduva'],
    scripts=['bin/brevduva'],
    install_requires=['distribute', 'imapclient'],
    url='https://github.com/derdon/brevduva',
)
