import tempfile
from StringIO import StringIO

import pytest

from brevduva.gui import MainWindow


@pytest.fixture
def main_window():
    return MainWindow(StringIO())


def test_window_init(main_window):
    assert main_window.active_accounts == {}
    assert main_window.cur_account is None
    # TODO: check that the folder tree and mail list are empty
