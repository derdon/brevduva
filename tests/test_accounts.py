import re
from imaplib import IMAP4_PORT, IMAP4_SSL_PORT
from StringIO import StringIO
from ConfigParser import NoOptionError

import pytest

from brevduva.accounts import Account, Accounts, ConfigBasedAccountManager,\
    AccountAlreadyExistsError, AccountNotFoundError


EXAMPLE_CONFIG = """[section]
name = bob
imapserver = imap.foo.com
ssl = no"""


@pytest.fixture
def no_accounts():
    return Accounts({})


@pytest.fixture
def work_account():
    return Account("imap.work.com", IMAP4_SSL_PORT, "bob", False, True)


@pytest.fixture
def some_accounts():
    return Accounts({
        "alice@home.com": Account("imap.home.com", IMAP4_PORT, "alice", True, False),
        "bob@work.com": work_account()})


@pytest.fixture
def empty_config():
    return ConfigBasedAccountManager(StringIO())


@pytest.fixture
def section_only_config():
    return ConfigBasedAccountManager(StringIO("[section]"))


@pytest.fixture
def missing_port_ssl_config():
    config = """[section]
name = bob
imapserver = imap.foo.com"""
    return ConfigBasedAccountManager(StringIO(config))


@pytest.fixture
def missing_port_no_ssl_config():
    return ConfigBasedAccountManager(StringIO(EXAMPLE_CONFIG))


@pytest.fixture
def read_config():
    config_manager = ConfigBasedAccountManager(StringIO(EXAMPLE_CONFIG))
    config_manager.read_config()
    return config_manager


class TestAccountExists(object):
    def test_no_accounts(self, no_accounts):
        assert 'doesnotexist' not in no_accounts

    def test_account_exists(self, some_accounts):
        assert 'alice@home.com' in some_accounts
        assert 'bob@work.com' in some_accounts


class TestAddAccount(object):
    def test_with_overwrite(self, no_accounts):
        account = Account('imap.foo.com', IMAP4_PORT, 'foo', True, False)
        no_accounts.add('default', account)
        assert 'default' in no_accounts
        assert no_accounts == {'default': account}

    def test_add_already_existing(self, some_accounts, work_account):
        with pytest.raises(AccountAlreadyExistsError):
            some_accounts.add('bob@work.com', work_account)


class TestRemoveAccount(object):
    def test_existing(self, some_accounts, work_account):
        popped_account = some_accounts.pop('bob@work.com')
        assert popped_account == work_account

    def test_nonexisting(self, no_accounts):
        with pytest.raises(AccountNotFoundError):
            no_accounts.remove('work')


class TestEditAccount(object):
    def test_existing(self, some_accounts, work_account):
        some_accounts.edit('alice@home.com', work_account)
        assert some_accounts['alice@home.com'] == work_account

    def test_nonexisting(self, no_accounts, work_account):
        with pytest.raises(AccountNotFoundError):
            no_accounts.edit('bob@work.com', work_account)


class TestReadConfig(object):
    def test_empty_config(self, empty_config):
        assert empty_config.accounts == Accounts()
        empty_config.read_config()
        assert empty_config.accounts == {}

    def test_section_only(self, section_only_config):
        assert section_only_config.accounts == Accounts()
        with pytest.raises(NoOptionError) as e:
            section_only_config.read_config()
        # example for error message is on the following line:
        #   No option u'name' in section: 'section'
        # test checks for RE instead of checking for equality with hard-coded
        # string because the order of read options is implementation-dependent
        # and thus not specified
        pattern = "No option u'(\w+)' in section: 'section'"
        assert re.match(pattern, e.value.message) is not None

    def test_missing_port_SSL(self, missing_port_ssl_config):
        assert missing_port_ssl_config.accounts == Accounts()
        missing_port_ssl_config.read_config()
        expected_account = Account(
            "imap.foo.com", IMAP4_SSL_PORT, "bob", False, True)
        expected_accounts = {"section": expected_account}
        assert missing_port_ssl_config.accounts == expected_accounts

    def test_missing_port_no_SSL(self, missing_port_no_ssl_config):
        assert missing_port_no_ssl_config.accounts == Accounts()
        missing_port_no_ssl_config.read_config()
        expected_account = Account(
            "imap.foo.com", IMAP4_PORT, "bob", False, False)
        expected_accounts = {"section": expected_account}
        assert missing_port_no_ssl_config.accounts == expected_accounts


class TestAddConfigAccount(object):
    def test_failing_overwite(self, read_config):
        assert read_config.accounts == {"section": Account(
            "imap.foo.com", IMAP4_PORT, "bob", False, False)}
        read_config.add("bob", Account("imap.foo.com", 42, "bob2", True, True))
